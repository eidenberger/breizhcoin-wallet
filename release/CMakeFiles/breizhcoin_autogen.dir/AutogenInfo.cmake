# Meta
set(AM_MULTI_CONFIG "SINGLE")
# Directories and files
set(AM_CMAKE_BINARY_DIR "/Users/manueleidenberger/breizhcoinwallet/release/")
set(AM_CMAKE_SOURCE_DIR "/Users/manueleidenberger/breizhcoinwallet/")
set(AM_CMAKE_CURRENT_SOURCE_DIR "/Users/manueleidenberger/breizhcoinwallet/")
set(AM_CMAKE_CURRENT_BINARY_DIR "/Users/manueleidenberger/breizhcoinwallet/release/")
set(AM_CMAKE_INCLUDE_DIRECTORIES_PROJECT_BEFORE "")
set(AM_BUILD_DIR "/Users/manueleidenberger/breizhcoinwallet/release/breizhcoin_autogen")
set(AM_SOURCES "/Users/manueleidenberger/breizhcoinwallet/src/CommandLineParser.cpp;/Users/manueleidenberger/breizhcoinwallet/src/CryptoNoteWrapper.cpp;/Users/manueleidenberger/breizhcoinwallet/src/CurrencyAdapter.cpp;/Users/manueleidenberger/breizhcoinwallet/src/LoggerAdapter.cpp;/Users/manueleidenberger/breizhcoinwallet/src/NodeAdapter.cpp;/Users/manueleidenberger/breizhcoinwallet/src/Settings.cpp;/Users/manueleidenberger/breizhcoinwallet/src/SignalHandler.cpp;/Users/manueleidenberger/breizhcoinwallet/src/WalletAdapter.cpp;/Users/manueleidenberger/breizhcoinwallet/src/gui/AboutDialog.cpp;/Users/manueleidenberger/breizhcoinwallet/src/gui/AddressBookDialog.cpp;/Users/manueleidenberger/breizhcoinwallet/src/gui/AddressBookFrame.cpp;/Users/manueleidenberger/breizhcoinwallet/src/gui/AddressBookModel.cpp;/Users/manueleidenberger/breizhcoinwallet/src/gui/AnimatedLabel.cpp;/Users/manueleidenberger/breizhcoinwallet/src/gui/ChangePasswordDialog.cpp;/Users/manueleidenberger/breizhcoinwallet/src/gui/ExitWidget.cpp;/Users/manueleidenberger/breizhcoinwallet/src/gui/MainWindow.cpp;/Users/manueleidenberger/breizhcoinwallet/src/gui/MainWindow.mm;/Users/manueleidenberger/breizhcoinwallet/src/gui/NewAddressDialog.cpp;/Users/manueleidenberger/breizhcoinwallet/src/gui/NewPasswordDialog.cpp;/Users/manueleidenberger/breizhcoinwallet/src/gui/OverviewFrame.cpp;/Users/manueleidenberger/breizhcoinwallet/src/gui/PasswordDialog.cpp;/Users/manueleidenberger/breizhcoinwallet/src/gui/ReceiveFrame.cpp;/Users/manueleidenberger/breizhcoinwallet/src/gui/RecentTransactionsModel.cpp;/Users/manueleidenberger/breizhcoinwallet/src/gui/SendFrame.cpp;/Users/manueleidenberger/breizhcoinwallet/src/gui/SortedTransactionsModel.cpp;/Users/manueleidenberger/breizhcoinwallet/src/gui/TransactionDetailsDialog.cpp;/Users/manueleidenberger/breizhcoinwallet/src/gui/TransactionFrame.cpp;/Users/manueleidenberger/breizhcoinwallet/src/gui/TransactionsFrame.cpp;/Users/manueleidenberger/breizhcoinwallet/src/gui/TransactionsListModel.cpp;/Users/manueleidenberger/breizhcoinwallet/src/gui/TransactionsModel.cpp;/Users/manueleidenberger/breizhcoinwallet/src/gui/TransferFrame.cpp;/Users/manueleidenberger/breizhcoinwallet/src/main.cpp")
set(AM_HEADERS "/Users/manueleidenberger/breizhcoinwallet/src/CommandLineParser.h;/Users/manueleidenberger/breizhcoinwallet/src/CryptoNoteWrapper.h;/Users/manueleidenberger/breizhcoinwallet/src/CurrencyAdapter.h;/Users/manueleidenberger/breizhcoinwallet/src/LoggerAdapter.h;/Users/manueleidenberger/breizhcoinwallet/src/NodeAdapter.h;/Users/manueleidenberger/breizhcoinwallet/src/Settings.h;/Users/manueleidenberger/breizhcoinwallet/src/SignalHandler.h;/Users/manueleidenberger/breizhcoinwallet/src/WalletAdapter.h;/Users/manueleidenberger/breizhcoinwallet/src/gui/AboutDialog.h;/Users/manueleidenberger/breizhcoinwallet/src/gui/AddressBookDialog.h;/Users/manueleidenberger/breizhcoinwallet/src/gui/AddressBookFrame.h;/Users/manueleidenberger/breizhcoinwallet/src/gui/AddressBookModel.h;/Users/manueleidenberger/breizhcoinwallet/src/gui/AnimatedLabel.h;/Users/manueleidenberger/breizhcoinwallet/src/gui/ChangePasswordDialog.h;/Users/manueleidenberger/breizhcoinwallet/src/gui/ExitWidget.h;/Users/manueleidenberger/breizhcoinwallet/src/gui/MainWindow.h;/Users/manueleidenberger/breizhcoinwallet/src/gui/NewAddressDialog.h;/Users/manueleidenberger/breizhcoinwallet/src/gui/NewPasswordDialog.h;/Users/manueleidenberger/breizhcoinwallet/src/gui/OverviewFrame.h;/Users/manueleidenberger/breizhcoinwallet/src/gui/PasswordDialog.h;/Users/manueleidenberger/breizhcoinwallet/src/gui/ReceiveFrame.h;/Users/manueleidenberger/breizhcoinwallet/src/gui/RecentTransactionsModel.h;/Users/manueleidenberger/breizhcoinwallet/src/gui/SendFrame.h;/Users/manueleidenberger/breizhcoinwallet/src/gui/SortedTransactionsModel.h;/Users/manueleidenberger/breizhcoinwallet/src/gui/TransactionDetailsDialog.h;/Users/manueleidenberger/breizhcoinwallet/src/gui/TransactionFrame.h;/Users/manueleidenberger/breizhcoinwallet/src/gui/TransactionsFrame.h;/Users/manueleidenberger/breizhcoinwallet/src/gui/TransactionsListModel.h;/Users/manueleidenberger/breizhcoinwallet/src/gui/TransactionsModel.h;/Users/manueleidenberger/breizhcoinwallet/src/gui/TransferFrame.h;/Users/manueleidenberger/breizhcoinwallet/src/gui/WalletEvents.h;/Users/manueleidenberger/breizhcoinwallet/src/miniupnpcstrings.h")
# Qt environment
set(AM_QT_VERSION_MAJOR "5")
set(AM_QT_VERSION_MINOR "10")
set(AM_QT_MOC_EXECUTABLE "/Users/manueleidenberger/Qt5.10/5.10.0/clang_64/bin/moc")
set(AM_QT_UIC_EXECUTABLE )
set(AM_QT_RCC_EXECUTABLE )
# MOC settings
set(AM_MOC_SKIP "/Users/manueleidenberger/breizhcoinwallet/release/breizhcoin_autogen/mocs_compilation.cpp;/Users/manueleidenberger/breizhcoinwallet/release/cryptonote_autogen/mocs_compilation.cpp")
set(AM_MOC_DEFINITIONS "GIT_REVISION=\"a42d92e\";QT_CORE_LIB;QT_GUI_LIB;QT_NO_DEBUG;QT_PRINTSUPPORT_LIB;QT_WIDGETS_LIB;_GNU_SOURCE")
set(AM_MOC_INCLUDES "/Users/manueleidenberger/breizhcoinwallet/release/breizhcoin_autogen/include;/Users/manueleidenberger/breizhcoinwallet/release;/Users/manueleidenberger/breizhcoinwallet/src;/Users/manueleidenberger/breizhcoinwallet/cryptonote/external;/Users/manueleidenberger/breizhcoinwallet/cryptonote/include;/Users/manueleidenberger/breizhcoinwallet/cryptonote/src;/usr/local/include;/usr/include/malloc;/Users/manueleidenberger/breizhcoinwallet/cryptonote/src/Platform/OSX;/Users/manueleidenberger/Qt5.10/5.10.0/clang_64/lib/QtPrintSupport.framework;/Users/manueleidenberger/Qt5.10/5.10.0/clang_64/lib/QtPrintSupport.framework/Headers;/Users/manueleidenberger/Qt5.10/5.10.0/clang_64/lib/QtWidgets.framework;/Users/manueleidenberger/Qt5.10/5.10.0/clang_64/lib/QtWidgets.framework/Headers;/Users/manueleidenberger/Qt5.10/5.10.0/clang_64/lib/QtGui.framework;/Users/manueleidenberger/Qt5.10/5.10.0/clang_64/lib/QtGui.framework/Headers;/Users/manueleidenberger/Qt5.10/5.10.0/clang_64/lib/QtCore.framework;/Users/manueleidenberger/Qt5.10/5.10.0/clang_64/lib/QtCore.framework/Headers;/Users/manueleidenberger/Qt5.10/5.10.0/clang_64/./mkspecs/macx-clang;/System/Library/Frameworks/OpenGL.framework/Headers")
set(AM_MOC_OPTIONS "")
set(AM_MOC_RELAXED_MODE "FALSE")
set(AM_MOC_MACRO_NAMES "Q_OBJECT;Q_GADGET;Q_NAMESPACE")
set(AM_MOC_DEPEND_FILTERS "")
set(AM_MOC_PREDEFS_CMD "")
# UIC settings
set(AM_UIC_SKIP )
set(AM_UIC_TARGET_OPTIONS )
set(AM_UIC_OPTIONS_FILES )
set(AM_UIC_OPTIONS_OPTIONS )
set(AM_UIC_SEARCH_PATHS )
# RCC settings
set(AM_RCC_SOURCES )
set(AM_RCC_BUILDS )
set(AM_RCC_OPTIONS )
set(AM_RCC_INPUTS )
# Configurations options
set(AM_CONFIG_SUFFIX_Release "_Release")
