**1. Clone wallet sources**

```
git clone https://github.com/cryptonotefoundation/cryptonotewallet.git
```

**2. Modify `CryptoNoteWallet.cmake`**
 
```
set(CN_PROJECT_NAME "furiouscoin")
set(CN_CURRENCY_DISPLAY_NAME "FuriousCoin")
set(CN_CURRENCY_TICKER "XFC")
```

**3. Set symbolic link to coin sources at the same level as `src`. For example:**

```
ln -s ../cryptonote cryptonote
```

Alternative way is to create git submodule:

```
git submodule add https://github.com/cryptonotefoundation/cryptonote.git cryptonote
```

Replace URL with git remote repository of your coin.

**4. Build**

```
mkdir build && cd build && cmake .. && make
```

Cmake maybe needs some paths to Qt libs... do : 

```
cmake -DQt5Gui_DIR=/Users/manueleidenberger/Qt5.10/5.10.0/clang_64/lib/cmake/Qt5Gui/ -DQt5Widgets_DIR=/Users/manueleidenberger/Qt5.10/5.10.0/clang_64/lib/cmake/Qt5Widgets/ -DQt5PrintSupport_DIR=/Users/manueleidenberger/Qt5.10/5.10.0/clang_64/lib/cmake/Qt5PrintSupport/ 
```

Deploy for mac : 

```
~/Qt5.10/5.10.0/clang_64/bin/macdeployqt breizhcoin.app -dmg
```